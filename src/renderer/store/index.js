import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)


export default new Vuex.Store({
    state: {
        height_from: 0,  // height refreshes
        height_to: 0,
        wallet_dir: "",
        created_wallet: {},  // only used when creating wallets
        appState: "",  // not really used
        error: "",
        message_box: {
            'title': '',
            'message': ''
        },
        usd_rate: 0,  // per 1000 WOW
        wallet: {
            'path': '',
            'txs': [],
            'balance': 0,
            'unlocked': 0,
            'address': '',
            'usd': 0
        }, // wallet opened
        wallet_path: '',
        wallet_password: ''
    },
    mutations: {
        addCreatedWallet({ created_wallet }, data) {
            created_wallet.seed = data.seed;
            created_wallet.address = data.address;
            created_wallet.view_key = data.view_key;
            created_wallet.name = data.name;
            created_wallet.password = data.password;
        },
        addWalletDir(state, data){
            state.wallet_dir = data;
        },
        appState(state, data){
            state.appState = data;
        },
        addRate(state, data){
            state.usd_rate = data;
        },
        showError(state, data){
            state.error = data;
        },
        showMessage(state, data){
            state.message_box.title = data.title;
            state.message_box.message = data.message;
        },
        addHeightRefresh(state, data){
            state.height_from = data.from;
            state.height_to = data.to;
        },
        addWallet({wallet}, data){
            if(data.hasOwnProperty('wallet_path')){
                wallet.path = data.wallet_path;
            }

            if(data.hasOwnProperty('txs')){
                console.log('setting store: txs')
                wallet.txs = data.txs;
            }

            if(data.hasOwnProperty('balance')){
                wallet.balance = data.balance;
            }

            if(data.hasOwnProperty('unlocked')){
                wallet.unlocked = data.unlocked;
            }

            if(data.hasOwnProperty('address')){
                wallet.address = data.address;
            }
        },
        addWalletPath(state, path){
            state.wallet_path = path;
        },
        addWalletPassword(state, password){
            state.wallet_password = password;
        }
    },
    getters: {
        created_wallet: state => state.created_wallet,
        wallet_dir: state => state.wallet_dir,
        appState: state => state.appState,
        error: state => state.error,
        wallet: state => state.wallet,
        wallet_path: state => state.wallet_path,
        wallet_password: state => state.wallet_password,
        usd_rate: state => state.usd_rate,
        message_box: state => state.message_box,
        height_from: state => state.height_from,
        height_to: state => state.height_to
    }
});
